enum PageNameEnum {

    HOME = "/home",

    DATA_BROWSE = "/data-browse",

    BASE_SEARCH = "/base-search",

    SENIOR_SEARCH = "/senior-search",

    DASHBOARD = '/dashboard',

    DASHBOARD_INFO = '/dashboard/info',

    DASHBOARD_NODE = '/dashboard/node',

    DASHBOARD_SHARD_AND_REPLICA = '/dashboard/shard-and-replica',

    SETTING = "/setting",

    SETTING_GLOBAL = "/setting/global",

    SETTING_URL = "/setting/url",

    SETTING_BACKUP = "/setting/backup",

    SETTING_SENIOR_FILTER_RECORD = "/setting/senior-filter-record",

    MORE = "/more",

    MORE_UPDATE = "/more/update",

    MORE_PRIVACY = "/more/privacy",

    MORE_ABOUT = "/more/about"

}

export default PageNameEnum;
